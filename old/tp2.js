'use strict';
process.stdin.setEncoding('utf8');

let secret = Math.floor((Math.random() * 10) + 1);
process.stdin.on('readable', () => {
    let chunk = process.stdin.read();
    chunk = Number(chunk);
    if (chunk !== null && chunk !== 0 && typeof(chunk) === "number") {
        if (chunk < secret) {
            console.log("plus haut");
        } else if (chunk > secret) {
            console.log("plus bas");
        }
        else if (chunk === secret) {
            console.log("Gagné !");
            process.exit();
        }
    }
    else {
        console.log("entrez un nombre svp : ");
    }
});

