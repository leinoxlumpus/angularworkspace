'use strict';
process.stdin.setEncoding('utf8');

console.log("Penses à un nombre entre 1 et 20, je vais le deviner !")
let secret = Math.floor((Math.random() * 20) + 1);
process.stdin.on('readable', () => {
    let chunk = process.stdin.read();
    if (chunk !== null) {
        if (chunk === "+\n") {
            secret = Math.floor((Math.random() * (20 - secret)) + secret + 1); 
            console.log(`tu pense au nombre : ${secret}`);
        } else if (chunk === "-\n") {
            secret = Math.floor((Math.random() * (secret - 1)) + 1); 
            console.log(`tu pense au nombre : ${secret}`);
        }
        else if (chunk === "=\n") {
            console.log("Gagné !");
            process.exit();
        }
        else {
            console.log("s'il te plait, utilise les signes +, - ou =");
        }
    }
    else {
        console.log(`tu pense au nombre : ${secret}`);
    }
});

